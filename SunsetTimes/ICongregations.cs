﻿using System;
using System.Collections.Generic;

namespace SunsetTimes
{
    public interface ICongregations
    {
      IDictionary<String, ICongregation> AllCongregations { get; set; }
    }
}
