﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunsetTimes
{
    public interface IChurchDate
    {
         DateTime ChurchServiceDate { get; set; }
         bool IsDoubleServices { get; set; }
    }
}
