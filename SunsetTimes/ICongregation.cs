﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunsetTimes
{
    public interface ICongregation
    {
        double longitude { get; set; }
        double latitude { get; set; }
        String congregationName { get; set; }
        string[] speakers { get; set; }
    }
}
