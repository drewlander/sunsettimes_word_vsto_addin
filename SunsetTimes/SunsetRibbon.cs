﻿using Innovative.SolarCalculator;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Tools.Ribbon;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Forms;



namespace SunsetTimes
{
    public partial class SunsetRibbon
    {
        private MonthCalendar monthControl;
        private DateTime previousDate;
        private Form dateForm;
        private bool sunsetTimesV2;
        private ICongregation Columbia;
        private ICongregation Florence;
        private ICongregation Augusta;
        private List<ChurchDate> HolyDays;
        private System.Windows.Forms.ListBox ignoreDatesListBox;
        private List<DateTime> ignoreDatesList;
        private System.Windows.Forms.DataGridView holyDayGridView;
        private String congregation1Name;
        private String congregation2Name;
        private String congregation3Name;
        private Dictionary<String, Congregation> allCongregations;

        private void SunsetRibbon_Load(object sender, RibbonUIEventArgs e)
        {
            previousDate = DateTime.UtcNow.Date;
            sunsetTimesV2Checkbox.Checked = Properties.Settings.Default.SunsetTimesV2;
            sunsetTimesV2 = sunsetTimesV2Checkbox.Checked;
            SunsetTimeCongregation3Button.Label = "changed";
            allCongregations = new Dictionary<String, Congregation>();
            // InitializeHolyDays();
            allCongregations = GetCongregations();
            UpdateSunsetButtons();
        }

        private void InitializeAllCongregations()
        {
            for (int i = 1; i <= 3; i++)
            {
                var speakers = new string[] { };
                var congregation = new Congregation { congregationName = String.Format("congregation{0}Name", i), latitude = 0, longitude = 0, speakers = speakers };
                allCongregations[congregation.congregationName] = congregation;
               
            }
        }
        private void InitializeCongregations()
        {
            Columbia =  new Congregation { latitude  = 34.00071, longitude = -81.03481, congregationName= sunsetTimesV2 ? "Columbia" : "Columbia, SC" };
            Augusta = new Congregation { latitude  = 33.466667, longitude = -81.966667, congregationName = sunsetTimesV2 ? "Augusta" : "Augusta, GA" };
            Florence = new Congregation { latitude  = 34.195435, longitude = -79.762566, congregationName = sunsetTimesV2 ? "Florence" : "Florence, SC" };
        }
        
        
        private void SunsetTimeCongregation1Button_Click(object sender, RibbonControlEventArgs e)
        {

            ShowDateTimeForm(allCongregations["congregation1Name"].longitude, allCongregations["congregation1Name"].latitude, allCongregations["congregation1Name"].congregationName);
        }
        
        private void ShowDateTimeForm(double longitude, double latitude, string text)
        {
            if (dateForm == null) 
                { dateForm = new Form(); 
            }
            if (monthControl != null)
            {
                monthControl.Dispose();
            }
            monthControl = new MonthCalendar();
            
            if (previousDate != null)
            {
                monthControl.SetDate(previousDate);
            }
            dateForm.Controls.Add(monthControl);
            dateForm.Show();
            dateForm.Focus();
            dateForm.FormClosed += delegate(object sender, FormClosedEventArgs e) { OnFormCLosing(sender, e); };
            monthControl.MaxSelectionCount = 1;
            monthControl.DateSelected += delegate (object sender, System.Windows.Forms.DateRangeEventArgs ev) { monthCalendar_ValueChanged(sender, ev, longitude, latitude, text); };
        }
        private void OnFormCLosing(object sender, FormClosedEventArgs e)
        {
            dateForm = null;
        }
        private void monthCalendar_ValueChanged(object sender, System.Windows.Forms.DateRangeEventArgs e, double longitude, double latitude, string text)
        {
            previousDate = monthControl.SelectionRange.Start.Date;
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeZoneInfo est = TimeZoneInfo.FindSystemTimeZoneById(localZone.StandardName.ToString());
            SolarTimes solarTimes = new SolarTimes(monthControl.SelectionRange.Start.Date, latitude, longitude);
            DateTime sunset = TimeZoneInfo.ConvertTimeFromUtc(solarTimes.Sunset.ToUniversalTime(), est);
            //not efficient but whatevah
            var sunsetTime = sunset.ToShortTimeString().ToLower();
            sunsetTime = sunsetTime.Replace("am", "a.m.");
            sunsetTime = sunsetTime.Replace("pm", "p.m.");
            var sunsetText = sunsetTimesV2 ? text + " - "+sunsetTime : text + " (Sunset- " + sunsetTime + ")";
            Globals.ThisAddIn.Application.Selection.Range.Text = sunsetText;
            Globals.ThisAddIn.Application.Selection.SelectCell();
            var test = Globals.ThisAddIn.Application.ActiveDocument.Paragraphs;
        }   


        private void SunsetTimeCongregation2Button_Click(object sender, RibbonControlEventArgs e)
        {
            //var congregation = sunsetTimesV2 ? "Augusta" : "Augusta, GA";
            ShowDateTimeForm(allCongregations["congregation2Name"].longitude, allCongregations["congregation2Name"].latitude, allCongregations["congregation2Name"].congregationName);
        }

        private void SunsetTimeCongregation3Button_Click(object sender, RibbonControlEventArgs e)
        {
            //var congregation = sunsetTimesV2 ? "Columbia" : "Columbia, SC";
            ShowDateTimeForm(allCongregations["congregation3Name"].longitude, allCongregations["congregation3Name"].latitude, allCongregations["congregation3Name"].congregationName);
        }
        private String GetSunestTime(DateTime sunsetDate, ICongregation Congregation)
        {
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeZoneInfo est = TimeZoneInfo.FindSystemTimeZoneById(localZone.StandardName.ToString());
            SolarTimes solarTimes = new SolarTimes(sunsetDate.AddDays(-1), Congregation.latitude, Congregation.longitude);
            DateTime sunset = TimeZoneInfo.ConvertTimeFromUtc(solarTimes.Sunset.ToUniversalTime(), est);
            //not efficient but whatevah
            var sunsetTime = sunset.ToShortTimeString().ToLower();
            sunsetTime = sunsetTime.Replace("am", "a.m.");
            sunsetTime = sunsetTime.Replace("pm", "p.m.");
            var sunsetText = sunsetTimesV2 ? Congregation.congregationName + " - " + sunsetTime : Congregation.congregationName + " (Sunset- " + sunsetTime + ")";
            return sunsetText;

        }

        //green
        private void button1_Click_1(object sender, RibbonControlEventArgs e)
        {
            change_cell_color(Color.FromArgb(0, 226, 239, 217));
        }

        //blue
        private void button2_Click(object sender, RibbonControlEventArgs e)
        {
            change_cell_color(Color.FromArgb(0, 217, 226, 243));
        }

        //orange
        private void button3_Click(object sender, RibbonControlEventArgs e)
        {
            change_cell_color(Color.FromArgb(0, 255, 242, 204));
        }

        //nocolor
        private void button3_Click_1(object sender, RibbonControlEventArgs e)
        {
            change_cell_color(Color.FromArgb(0, 253, 253, 253));
        }

        //yellow
        private void button4_Click(object sender, RibbonControlEventArgs e)
        {
            change_cell_color(Color.FromArgb(0, 255, 255, 0));
        }

        //dark grey
        private void button5_Click(object sender, RibbonControlEventArgs e)
        {
            change_cell_color(Color.FromArgb(0, 89, 89, 89));
        }

        //light grey
        private void lightGreyButton_Click(object sender, RibbonControlEventArgs e)
        {
            change_cell_color(Color.FromArgb(0, 242, 242, 242));
        }

        private void change_cell_color(Color color)
        {
            int cellCount = Globals.ThisAddIn.Application.Selection.Range.Cells.Count;
            var temp = Globals.ThisAddIn.Application.ActiveWindow.Selection.Range.Cells.Count;
            var range = Globals.ThisAddIn.Application.Selection.Cells;
            range.Shading.BackgroundPatternColor = (WdColor)ColorTranslator.ToOle(color);
            int selectedRowIndex = Globals.ThisAddIn.Application.Selection.Range.Cells[1].RowIndex;
        }
        private void teenyastudybutton_Click(object sender, RibbonControlEventArgs e)
        {
            var selectionForm = new Form();
            var speakersComboBox = new System.Windows.Forms.ComboBox();

            string[] speakers = new string[] { "Van Hanson", "Johnathan Arnold", "Andrew Tranquada", "Larry Lambert" };
            speakersComboBox.Items.AddRange(speakers);
            selectionForm.Controls.Add(speakersComboBox);
            speakersComboBox.SelectedIndexChanged +=
            new System.EventHandler(speakersComboBox_SelectedIndexChanged);
            selectionForm.Show();
            
        }

        private void speakersComboBox_SelectedIndexChanged(object sender,
     System.EventArgs e)
        {
            System.Windows.Forms.ComboBox comboBox = (System.Windows.Forms.ComboBox)sender;
            string selectedSpeaker = (string)comboBox.SelectedItem;
            Globals.ThisAddIn.Application.Selection.Range.Text = "Teen / YA Bible Study - " + selectedSpeaker; ;
        }
        

            private void youthclass1button_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.Application.Selection.Range.Text = "Youth Class Level 1";
        }

        private void youthclass24button_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.Application.Selection.Range.Text = "Youth Class Level 2&4";
        }

        private void teenpreeteenbutton_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.Application.Selection.Range.Text = " Elementary / Preteen Class";
        }

        private void teenbutton_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.Application.Selection.Range.Text = "Teen Class";
        }

        private void sunsetTimesV2Checkbox_Click(object sender, RibbonControlEventArgs e)
        {
            Properties.Settings.Default.SunsetTimesV2 = sunsetTimesV2Checkbox.Checked;
            sunsetTimesV2 = sunsetTimesV2Checkbox.Checked;
        }

        private void SunsetRibbon_Close(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void mensclubButton_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.Application.Selection.Range.Text = "Men's Club";
        }

        private void FindReplace(String findText, String replacementText) 
        {
            Microsoft.Office.Interop.Word.Range rngFind = Globals.ThisAddIn.Application.Selection.Range;
            Microsoft.Office.Interop.Word.Find fnd = rngFind.Find;
            fnd.ClearFormatting();
            fnd.Text = findText;
            fnd.Replacement.Text = replacementText;
            fnd.Replacement.ClearFormatting();
            //fnd.Replacement.Font.Bold = -1;
            //fnd.Replacement.Font.Italic = 0;
           // fnd.Replacement.Font.Underline = 0;
            fnd.Forward = true;
            fnd.Wrap = Microsoft.Office.Interop.Word.WdFindWrap.wdFindStop;
            fnd.Format = true;
            fnd.MatchCase = true;
            fnd.MatchWholeWord = false;
            fnd.MatchWildcards = false;
            fnd.MatchSoundsLike = false;
            fnd.MatchAllWordForms = false;

            fnd.Execute(Replace: Microsoft.Office.Interop.Word.WdReplace.wdReplaceOne);
        }

        private void GetCalendarDate()
        {
            DateTime projectStart = new DateTime(2022, 1, 11);
            DateTime projectEnd = new DateTime(2061, 2, 28);
            ignoreDatesList = new List<DateTime>();
            var dateForm = new Form();
            dateForm.Size = new System.Drawing.Size(560, 523);

            var datePickerStart = new DateTimePicker();
            datePickerStart.Location = new System.Drawing.Point(0, 0);

            var datePickerStartLabel = new System.Windows.Forms.Label();
            datePickerStartLabel.Text = "Start";
            datePickerStartLabel.Location = new System.Drawing.Point(221, 2);

            var datePickerEnd = new DateTimePicker();
            datePickerEnd.Location = new System.Drawing.Point(0, 20);

            var datePickerEndLabel = new System.Windows.Forms.Label();
            datePickerEndLabel.Text = "End";
            datePickerEndLabel.Location = new System.Drawing.Point(221, 22);

            var confirmButton = new System.Windows.Forms.Button();
            confirmButton.Text = "Generate";
            confirmButton.Location = new System.Drawing.Point(0, 40);

            ignoreDatesListBox = new System.Windows.Forms.ListBox();
            ignoreDatesListBox.Location = new System.Drawing.Point(0, 70);
            
            var addIgnoreDate = new System.Windows.Forms.Button();
            addIgnoreDate.Location = new System.Drawing.Point(120, 80);
            addIgnoreDate.Size = new System.Drawing.Size(160, 23);
            addIgnoreDate.Text = "Add Date To Ignore";
            //ignoreDatesComboBox.DataSource = ignoreDatesList;

            var removeIgnoreDate = new System.Windows.Forms.Button();
            removeIgnoreDate.Text = "Remove Date To Ignore";
            removeIgnoreDate.Size = new System.Drawing.Size(160, 23);
            removeIgnoreDate.Location = new System.Drawing.Point(120, 110);

            holyDayGridView = new System.Windows.Forms.DataGridView();
            holyDayGridView.Location = new System.Drawing.Point(0, 240);


            var newHolyDayButton = new System.Windows.Forms.Button();
            var deleteHolyDayButon = new System.Windows.Forms.Button();


            newHolyDayButton.Text = "Add Holy Day";
            newHolyDayButton.Location = new System.Drawing.Point(270, 250);
            newHolyDayButton.Size = new System.Drawing.Size(160, 23);
            newHolyDayButton.Click += delegate (object sender, EventArgs ev) { newHolyDayButton_Click(sender, ev, holyDayGridView); };

            deleteHolyDayButon.Text = "Delete HolyDay";
            deleteHolyDayButon.Location = new System.Drawing.Point(270, 290);
            deleteHolyDayButon.Size = new System.Drawing.Size(160, 23);
            deleteHolyDayButon.Click += delegate (object sender, EventArgs ev) {deleteHolyDayButton_Click(sender, ev, holyDayGridView);
        };;


            dateForm.Controls.Add(confirmButton);
            dateForm.Controls.Add(datePickerStart);
            dateForm.Controls.Add(datePickerEnd);
            dateForm.Controls.Add(datePickerStartLabel);
            dateForm.Controls.Add(datePickerEndLabel);
            dateForm.Controls.Add(ignoreDatesListBox);
            dateForm.Controls.Add(addIgnoreDate);
            dateForm.Controls.Add(removeIgnoreDate);
            setupHolyDayGridView(holyDayGridView);
            dateForm.Controls.Add(holyDayGridView);
            dateForm.Controls.Add(newHolyDayButton);
            dateForm.Controls.Add(deleteHolyDayButon);
            //dateForm.Controls.Add(buttonPanel);
            holyDayGridView.AllowUserToAddRows = false;
            holyDayGridView.ClearSelection();
            dateForm.Show();
            dateForm.Focus();
            

            confirmButton.Click += delegate (object sender, EventArgs ev) { OkButtonClicked(sender, ev, datePickerStart.Value, datePickerEnd.Value); };
            dateForm.FormClosed += delegate (object sender, FormClosedEventArgs e) { OnFormCLosing(sender, e); };
            addIgnoreDate.Click += delegate (object sender, EventArgs e) { AddIgnoreDate(sender, e); };
            removeIgnoreDate.Click += delegate (object sender, EventArgs e) { RemoveIgnoreDate(sender, e); };

        }

        DataGridViewCheckBoxColumn SetupHolyDayCheckBoxForGridView()
        {
            DataGridViewCheckBoxColumn column = new DataGridViewCheckBoxColumn();
            {
                column.HeaderText = "Double Services";
                column.Name = "Double Services";
                column.AutoSizeMode =
                    DataGridViewAutoSizeColumnMode.DisplayedCells;
                column.FlatStyle = FlatStyle.Standard;
                column.ThreeState = true;
                column.CellTemplate = new DataGridViewCheckBoxCell();
                column.CellTemplate.Style.BackColor = Color.Beige;
            }
            return column;
        }
        private void AddIgnoreDate(object sender, EventArgs e)
        {
            var dateForm = new Form();
            var ignoreDatePicker = new System.Windows.Forms.DateTimePicker();
            ignoreDatePicker.Location = new System.Drawing.Point(0, 0);
            var addButton = new System.Windows.Forms.Button();
            addButton.Location = new System.Drawing.Point(0, 20);
            addButton.Size = new System.Drawing.Size(160, 23);
            addButton.Text = "Add Ignore Date";
            dateForm.Controls.Add(ignoreDatePicker);
            dateForm.Controls.Add(addButton);
            dateForm.Show();
            addButton.Click += delegate (object s, EventArgs ev) { AddIgnoreDateToList(sender, ev, ignoreDatePicker.Value, ignoreDatePicker.Value); };
        }
        private void RemoveIgnoreDate(object sender, EventArgs e)
        {
            foreach (string s in ignoreDatesListBox.SelectedItems.OfType<string>().ToList())
                ignoreDatesListBox.Items.Remove(s);
        }

        private void AddIgnoreDateToList(object sender, EventArgs e, DateTime startDate, DateTime endDate)
        {
            ignoreDatesList.Add(startDate);
            ignoreDatesListBox.Items.Add(startDate.ToShortDateString());
        }
        private void setupHolyDayGridView(System.Windows.Forms.DataGridView holyDayGrid)
        {
            holyDayGrid.ColumnCount = 2;
            holyDayGrid.Name = "Holy Days";
            holyDayGrid.Size = new Size(260, 200);

            holyDayGrid.Columns[0].Name = "Date";
            holyDayGrid.Columns.Insert(1, SetupHolyDayCheckBoxForGridView());
            holyDayGrid.Columns[1].Name = "Double Services";
            holyDayGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            holyDayGrid.MultiSelect = false;

        }
        private void newHolyDayButton_Click(object sender, EventArgs e, System.Windows.Forms.DataGridView holyDayGridView)
        {
            var dateForm = new Form();
            var holyDayDatePicker = new System.Windows.Forms.DateTimePicker();
            holyDayDatePicker.Location = new System.Drawing.Point(0, 0);
            var addButton = new System.Windows.Forms.Button();
            addButton.Location = new System.Drawing.Point(0, 20);
            addButton.Size = new System.Drawing.Size(160, 23);
            addButton.Text = "Add Holy Day Date";

            var doubleServiceCheckBox = new System.Windows.Forms.CheckBox();
            doubleServiceCheckBox.Text = "Double Services";
            doubleServiceCheckBox.Location =  new System.Drawing.Point(0, 70);
            doubleServiceCheckBox.Size = new System.Drawing.Size(160, 23);
            dateForm.Controls.Add(holyDayDatePicker);
            dateForm.Controls.Add(addButton);
            dateForm.Controls.Add(doubleServiceCheckBox);
            dateForm.Show();
            addButton.Click += delegate (object s, EventArgs ev) { AddHolyDayToGridView(sender, ev, holyDayGridView, doubleServiceCheckBox, holyDayDatePicker.Value); };
        }

        private void AddHolyDayToGridView(object sender, EventArgs e, System.Windows.Forms.DataGridView holyDayGridView, System.Windows.Forms.CheckBox doubleServicesCheckBox, DateTime holyDay)
        {
            holyDayGridView.Rows.Add(holyDay.ToShortDateString(), doubleServicesCheckBox.Checked);
            var test = doubleServicesCheckBox.Checked;
        }
        private void deleteHolyDayButton_Click(object sender, EventArgs e, System.Windows.Forms.DataGridView holyDayGridView)
        {
            if (holyDayGridView.SelectedRows.Count > 0)
            {
                holyDayGridView.Rows.RemoveAt(
                    holyDayGridView.SelectedRows[0].Index);
            }
        }
        private void OkButtonClicked(object sender, EventArgs e, DateTime startDate, DateTime endDate)
        {
            //InitializeCongregations();
            InitializeHolyDays();
            List<IChurchDate> days_list = new List<IChurchDate>();
            //Add all Holy Days
            foreach (var holyDay in HolyDays)
            {
                if (holyDay.ChurchServiceDate > startDate)
                {
                    days_list.Add(holyDay);
                }
            }
            //Find All Saturdays
            for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
            {
                if (date.DayOfWeek == DayOfWeek.Saturday)
                    //Only add if a date does not already exist (it is a Holy Day)
                    if (days_list.Where(x => x.ChurchServiceDate.ToShortDateString() == date.ToShortDateString()).Count() == 0)
                    {
                        days_list.Add(new ChurchDate { ChurchServiceDate = date, IsDoubleServices = false });
                    }
            }
            //Sort dates 
            var sortedDaysFull = days_list.GroupBy(x => x.ChurchServiceDate).Select(x => x.First()).OrderBy(x => x.ChurchServiceDate).ToList();
            var sortedDays = DatesToIgnore(sortedDaysFull);
            // which number to replace in the Form
            var regexDays = 1;
            // Can we clean this up please?
            // TODO: Cleanup (we know this will never happen since I added a TODO)
            for (var i = 0; i < sortedDays.Count; i++)
            {
                string findDayString, findCongregation1SunsetString, findCongregation2SunsetString, findCongregation3SunsetString;
                ReplaceDatesAndSunsetTimes(sortedDays, regexDays, i, out findDayString, out findCongregation1SunsetString, out findCongregation2SunsetString, out findCongregation3SunsetString);
                regexDays++;
                if (sortedDays[i].IsDoubleServices)
                {
                    ReplaceDatesAndSunsetTimes(sortedDays, regexDays, i, out findDayString, out findCongregation1SunsetString, out findCongregation2SunsetString, out findCongregation3SunsetString);
                    regexDays++;
                }
            }
        }
        private Button CreateButton(string text, Size size, System.Drawing.Point point, string controlName)
        {
            return new Button{ Name = controlName, Size = size, Location = point, Text = text };
        }

        private TextBox CreateTextBox(string text, Size size, System.Drawing.Point point, string controlName)
        {
            return new TextBox { Name = controlName, Size = size, Location = point, Text = text };
        }

        private ComboBox CreateComboBox(string[] speakers, Size size, System.Drawing.Point point, string controlName)
        {
            var combo = new ComboBox();
            combo.Size = size;
            combo.Location = point;
            combo.Name = controlName;
            combo.Items.AddRange(speakers);
            return combo;

        }
        private Label CreateLabel(string text, Size size, System.Drawing.Point point, string controlName)
        {
            return new Label { Name = controlName, Size = size, Location = point, Text = text };
        }
        private void ReplaceDatesAndSunsetTimes(List<IChurchDate> sortedDays, int regexDays, int i, out string findDayString, out string findCongregation1String, out string findCongregation2SunsetString, out string findCongregation3SunsetString)
        {
            findDayString = String.Concat("Day", regexDays);
            findCongregation1String = String.Concat("Congregation1Sunset", regexDays);
            findCongregation2SunsetString = String.Concat("Congregation2Sunset", regexDays);
            findCongregation3SunsetString = String.Concat("Congregation3Sunset", regexDays);
            FindReplace(String.Concat("Month", regexDays), System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Parse(sortedDays[i].ChurchServiceDate.ToShortDateString()).Month));
            FindReplace(String.Concat("Day", regexDays), DateTime.Parse(sortedDays[i].ChurchServiceDate.ToShortDateString()).Day.ToString());
            FindReplace(String.Concat("Congregation1Sunset", regexDays), GetSunestTime(DateTime.Parse(sortedDays[i].ChurchServiceDate.ToShortDateString()), allCongregations["congregation1Name"]));
            FindReplace(String.Concat("Congregation2Sunset", regexDays), GetSunestTime(DateTime.Parse(sortedDays[i].ChurchServiceDate.ToShortDateString()), allCongregations["congregation2Name"]));
            FindReplace(String.Concat("Congregation3Sunset", regexDays), GetSunestTime(DateTime.Parse(sortedDays[i].ChurchServiceDate.ToShortDateString()), allCongregations["congregation3Name"]));
        }

        private void testButton_Click(object sender, RibbonControlEventArgs e)
        {
            GetCalendarDate();
        }

        private void InitializeHolyDays()
        {
            var dates = new List<String>();
            var doubleServicesDates = new List<String>();
            var fullDays = new List<ChurchDate>();
            foreach (DataGridViewRow dr in holyDayGridView.Rows)
                {
                if (String.Compare(dr.Cells["Double Services"].Value.ToString(), "True")==0) {
                    doubleServicesDates.Add(dr.Cells["Date"].Value.ToString());
                }
                else
                {
                    dates.Add(dr.Cells["Date"].Value.ToString());
                }
            }
           
            foreach(var holyDay in dates)
            {
                fullDays.Add(new ChurchDate { ChurchServiceDate = DateTime.ParseExact(holyDay, "M/d/yyyy", CultureInfo.InvariantCulture), IsDoubleServices = false });
            }
            foreach (var holyDay in doubleServicesDates)
            {
                fullDays.Add(new ChurchDate { ChurchServiceDate = DateTime.ParseExact(holyDay, "M/d/yyyy", CultureInfo.InvariantCulture), IsDoubleServices = true });
            }
            HolyDays = fullDays;

        }
        public List<IChurchDate> DatesToIgnore(List<IChurchDate> churchDates)
        {
            var datesToIgnore = ignoreDatesListBox.Items.Cast<String>();
            foreach (var dayToIgnore in datesToIgnore)
            {
                var ignoreDate = new ChurchDate { ChurchServiceDate = DateTime.ParseExact(dayToIgnore, "M/d/yyyy", CultureInfo.InvariantCulture) };
                churchDates.RemoveAll(x => x.ChurchServiceDate.ToShortDateString() == ignoreDate.ChurchServiceDate.ToShortDateString());
                var test = churchDates.Select(x => x.ChurchServiceDate.Date == DateTime.ParseExact(dayToIgnore, "M/d/yyyy", CultureInfo.InvariantCulture));
            }
            return churchDates;
        }

        private void configurationButton_Click(object sender, RibbonControlEventArgs e)
        {
            CreateConfiguration();
        }
        private void CreateConfiguration()
        {
            var all = GetCongregations();
            var configurationForm = new Form();
            configurationForm.Size = new System.Drawing.Size(560, 523);
            var createConfigurationButton = CreateButton("Save Configuration", new System.Drawing.Size(300, 30),
                new System.Drawing.Point(0, 0),"configurationButton");

            var congregation1Name = CreateTextBox(all["congregation1Name"].congregationName, new System.Drawing.Size(100, 20), 
                new System.Drawing.Point(0, 50), "congregation1Name");

            var congregation1NameLabel = CreateLabel("First Congregation's Name", new System.Drawing.Size(400, 20),
                new System.Drawing.Point(110, 50),"congregation1NameLabel");

            var congregation1Coordinates = CreateTextBox(String.Format("{0},{1}", all["congregation1Name"].latitude, all["congregation1Name"].longitude), new System.Drawing.Size(100, 20),
                new System.Drawing.Point(0, 75), "congregation1Coordinates");

            var congregation1CoordinatesLabel = CreateLabel("First Congregation's Coordinates (lat,long)", new System.Drawing.Size(400, 20),
                new System.Drawing.Point(110, 75), "congregation1CoordinatesLabel");

            var congregation1Speakers = CreateComboBox(all["congregation1Name"].speakers, new System.Drawing.Size(400, 300),
                new System.Drawing.Point(300, 100), "congregation1Speakers");
            //new System.EventHandler(speakersComboBox_SelectedIndexChanged);

            var congregation2Name = CreateTextBox(all["congregation2Name"].congregationName, new System.Drawing.Size(100, 20),
    new System.Drawing.Point(0, 100), "congregation2Name");

            var congregation2NameLabel = CreateLabel("Second Congregation's Name", new System.Drawing.Size(400, 20),
                new System.Drawing.Point(110, 100), "congregation2NameLabel");

            var congregation2Coordinates = CreateTextBox(String.Format("{0},{1}", all["congregation2Name"].latitude, all["congregation2Name"].longitude), new System.Drawing.Size(100, 20),
              new System.Drawing.Point(0, 125), "congregation2Coordinates");

            var congregation2CoordinatesLabel = CreateLabel("Second Congregation's Coordinates (lat,long)", new System.Drawing.Size(400, 20),
                new System.Drawing.Point(110, 125), "congregation2CoordinatesLabel");


            var congregation3Name = CreateTextBox(all["congregation3Name"].congregationName, new System.Drawing.Size(100, 20),
   new System.Drawing.Point(0, 150), "congregation3Name");

            var congregation3NameLabel = CreateLabel("Third Congregation's Name", new System.Drawing.Size(400, 20),
                new System.Drawing.Point(110, 150), "congregation3NameLabel");

            var congregation3Coordinates = CreateTextBox(String.Format("{0},{1}", all["congregation3Name"].latitude, all["congregation3Name"].longitude), new System.Drawing.Size(100, 20),
              new System.Drawing.Point(0, 175), "congregation3Coordinates");

            var congregation3CoordinatesLabel = CreateLabel("Third Congregation's Coordinates (lat,long)", new System.Drawing.Size(400, 20),
                new System.Drawing.Point(110, 175), "congregation3CoordinatesLabel");


            congregation1Name.TextChanged += delegate (object sender, EventArgs ev) { CongregationNameTextBoxChanged(sender, ev, congregation1Name); };
            congregation2Name.TextChanged += delegate (object sender, EventArgs ev) { CongregationNameTextBoxChanged(sender, ev, congregation2Name); };
            congregation3Name.TextChanged += delegate (object sender, EventArgs ev) { CongregationNameTextBoxChanged(sender, ev, congregation3Name); };

            congregation1Coordinates.TextChanged += delegate (object sender, EventArgs ev) { CongregationCoordinateTextBoxChanged(sender, ev, congregation1Name); };
            congregation2Coordinates.TextChanged += delegate (object sender, EventArgs ev) { CongregationCoordinateTextBoxChanged(sender, ev, congregation2Name); };
            congregation3Coordinates.TextChanged += delegate (object sender, EventArgs ev) { CongregationCoordinateTextBoxChanged(sender, ev, congregation3Name); };

            createConfigurationButton.Click += delegate (object sender, EventArgs ev) { createConfigurationButtonClicked(sender, ev); };

            configurationForm.FormClosed += delegate (object sender, FormClosedEventArgs e) { OnConfigurationFormCLosing(sender, e); };

            configurationForm.Controls.Add(createConfigurationButton);
            configurationForm.Controls.Add(congregation1Name);
            configurationForm.Controls.Add(congregation1Coordinates);
            configurationForm.Controls.Add(congregation1NameLabel);
            //configurationForm.Controls.Add(congregation1Speakers);
            configurationForm.Controls.Add(congregation1CoordinatesLabel);
            configurationForm.Controls.Add(congregation2Name);
            configurationForm.Controls.Add(congregation2Coordinates);
            configurationForm.Controls.Add(congregation2NameLabel);
            configurationForm.Controls.Add(congregation2CoordinatesLabel);
            configurationForm.Controls.Add(congregation3Name);
            configurationForm.Controls.Add(congregation3Coordinates);
            configurationForm.Controls.Add(congregation3NameLabel);
            configurationForm.Controls.Add(congregation3CoordinatesLabel);
            configurationForm.Show();
            configurationForm.Focus();
             
        }

        private void OnConfigurationFormCLosing(object sender, FormClosedEventArgs e)
        {
            DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Do you want to save any changes?", "Save Configuration", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.Yes)
            {
                SaveConfiguration(allCongregations);
            }
            /*else if (dialogResult == DialogResult.No)
            {
                //do something else
            }*/
            //SaveConfiguration(allCongregations);
        }

        private void CongregationNameTextBoxChanged(object sender, EventArgs e, System.Windows.Forms.TextBox textBox)
        {
            var y = allCongregations[textBox.Name];
            y.congregationName = textBox.Text;
        }
        private void CongregationCoordinateTextBoxChanged(object sender, EventArgs e, System.Windows.Forms.TextBox textBox)
        {
            var y = allCongregations[textBox.Name];
            var coordinates = sender as TextBox;
            Regex regexObj = new Regex(@"(?<lat>[-\d.]+),(?<long>[-\d.]+)");
            Match matchResult = regexObj.Match(coordinates.Text);
            while (matchResult.Success)
            {
                try
                {
                    y.latitude = Convert.ToDouble(matchResult.Groups["lat"].Value);

                    y.longitude = Convert.ToDouble(matchResult.Groups["long"].Value);
                }catch(FormatException ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                matchResult = matchResult.NextMatch();
            }
        }
        private void createConfigurationButtonClicked(object sender, EventArgs e)
        {
            SaveConfiguration(allCongregations);
        }
        private Dictionary<String, Congregation> GetCongregations()
        {
            var configFile = GetConfigurationFilePath();
            if (File.Exists(configFile))
            {
                return LoadCongregationsFromFile(configFile);
            }
            else
            {
                InitializeAllCongregations();
                var deserializedCongregations = Newtonsoft.Json.JsonConvert.SerializeObject(allCongregations);
                File.WriteAllText(configFile, deserializedCongregations);
                return allCongregations;
            }
        }
        private Dictionary<String, Congregation> LoadCongregationsFromFile(string config)
        {
                JsonSerializer serializer = new JsonSerializer();
                using (StreamReader r = new StreamReader(config))
                {
                    string json = r.ReadToEnd();
                    var ro = JsonConvert.DeserializeObject<Dictionary<String, Congregation>>(json);
                    Console.WriteLine(ro);
                    allCongregations = ro;
                    return ro;
                }
            
            //File.WriteAllText(String.Format("{0}/sunsetRibbon.json",assemblyDirectory));

        }
        private void SaveConfiguration(Dictionary<String, Congregation> congregations)
        {
            var filePath = GetConfigurationFilePath();

            var deserializedCongregations = Newtonsoft.Json.JsonConvert.SerializeObject(congregations);
            File.WriteAllText(filePath, deserializedCongregations);
            System.Windows.Forms.MessageBox.Show("Configuration save successfully!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            UpdateSunsetButtons();
        }

        private string GetConfigurationFilePath()
        {
            var assemblyDLL = System.Reflection.Assembly.GetExecutingAssembly();
            var assemblyDirectory = System.IO.Path.GetDirectoryName(assemblyDLL.Location);
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string fullDirectoryPath = String.Format("{0}/SunsetConfiguration", folder);
            Directory.CreateDirectory(fullDirectoryPath);
            return String.Format("{0}/SunsetConfiguration.json", fullDirectoryPath);
        }

        private void UpdateSunsetButtons()
        {
            SunsetTimeCongregation1Button.Label = allCongregations["congregation1Name"].congregationName;
            SunsetTimeCongregation2Button.Label = allCongregations["congregation2Name"].congregationName;
            SunsetTimeCongregation3Button.Label = allCongregations["congregation3Name"].congregationName;
        }
    }
    public class Congregations: ICongregations
    {
        public IDictionary<string, ICongregation> AllCongregations { get; set; }
    }
    class TeenYaSpeakers
    {
        public string Name { get; set; }
    }
    public class Congregation : ICongregation
    {
        public double longitude { get; set; }
        public double latitude { get; set; }
        public String congregationName { get; set; }
        public string[] speakers { get; set; }

    }
    public class ChurchDate : IChurchDate
    {
       public DateTime ChurchServiceDate { get; set ; }
       public  bool IsDoubleServices { get; set ; }
    }
}
