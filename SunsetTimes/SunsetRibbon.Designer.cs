﻿
namespace SunsetTimes
{
    partial class SunsetRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SunsetRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.SunsetRibbonControl = this.Factory.CreateRibbonGroup();
            this.SunsetTimeCongregation1Button = this.Factory.CreateRibbonButton();
            this.SunsetTimeCongregation2Button = this.Factory.CreateRibbonButton();
            this.SunsetTimeCongregation3Button = this.Factory.CreateRibbonButton();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.blueButton = this.Factory.CreateRibbonButton();
            this.greenButton = this.Factory.CreateRibbonButton();
            this.OrangeButton = this.Factory.CreateRibbonButton();
            this.nocolorbutton = this.Factory.CreateRibbonButton();
            this.yellowbutton = this.Factory.CreateRibbonButton();
            this.darkgreybutton = this.Factory.CreateRibbonButton();
            this.lightGreyButton = this.Factory.CreateRibbonButton();
            this.group3 = this.Factory.CreateRibbonGroup();
            this.teenyastudybutton = this.Factory.CreateRibbonButton();
            this.youthclass1button = this.Factory.CreateRibbonButton();
            this.youthclass24button = this.Factory.CreateRibbonButton();
            this.elementarypreeteenbutton = this.Factory.CreateRibbonButton();
            this.teenbutton = this.Factory.CreateRibbonButton();
            this.mensclubButton = this.Factory.CreateRibbonButton();
            this.group4 = this.Factory.CreateRibbonGroup();
            this.sunsetTimesV2Checkbox = this.Factory.CreateRibbonCheckBox();
            this.testButton = this.Factory.CreateRibbonButton();
            this.configurationButton = this.Factory.CreateRibbonButton();
            this.tab2 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.tab1.SuspendLayout();
            this.SunsetRibbonControl.SuspendLayout();
            this.group2.SuspendLayout();
            this.group3.SuspendLayout();
            this.group4.SuspendLayout();
            this.tab2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.SunsetRibbonControl);
            this.tab1.Groups.Add(this.group2);
            this.tab1.Groups.Add(this.group3);
            this.tab1.Groups.Add(this.group4);
            this.tab1.Label = "TabAddIns";
            this.tab1.Name = "tab1";
            // 
            // SunsetRibbonControl
            // 
            this.SunsetRibbonControl.Items.Add(this.SunsetTimeCongregation1Button);
            this.SunsetRibbonControl.Items.Add(this.SunsetTimeCongregation2Button);
            this.SunsetRibbonControl.Items.Add(this.SunsetTimeCongregation3Button);
            this.SunsetRibbonControl.Label = "Sunset Times";
            this.SunsetRibbonControl.Name = "SunsetRibbonControl";
            // 
            // SunsetTimeCongregation1Button
            // 
            this.SunsetTimeCongregation1Button.Label = "Sunset For Williamsburg";
            this.SunsetTimeCongregation1Button.Name = "SunsetTimeCongregation1Button";
            this.SunsetTimeCongregation1Button.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.SunsetTimeCongregation1Button_Click);
            // 
            // SunsetTimeCongregation2Button
            // 
            this.SunsetTimeCongregation2Button.Label = "Sunset For Charleston";
            this.SunsetTimeCongregation2Button.Name = "SunsetTimeCongregation2Button";
            this.SunsetTimeCongregation2Button.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.SunsetTimeCongregation2Button_Click);
            // 
            // SunsetTimeCongregation3Button
            // 
            this.SunsetTimeCongregation3Button.Label = "Sunset For Roanoke";
            this.SunsetTimeCongregation3Button.Name = "SunsetTimeCongregation3Button";
            this.SunsetTimeCongregation3Button.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.SunsetTimeCongregation3Button_Click);
            // 
            // group2
            // 
            this.group2.Items.Add(this.blueButton);
            this.group2.Items.Add(this.greenButton);
            this.group2.Items.Add(this.OrangeButton);
            this.group2.Items.Add(this.nocolorbutton);
            this.group2.Items.Add(this.yellowbutton);
            this.group2.Items.Add(this.darkgreybutton);
            this.group2.Items.Add(this.lightGreyButton);
            this.group2.Label = "Cell Shading";
            this.group2.Name = "group2";
            // 
            // blueButton
            // 
            this.blueButton.Label = "Blue";
            this.blueButton.Name = "blueButton";
            this.blueButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button2_Click);
            // 
            // greenButton
            // 
            this.greenButton.Label = "Green";
            this.greenButton.Name = "greenButton";
            this.greenButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button1_Click_1);
            // 
            // OrangeButton
            // 
            this.OrangeButton.Label = "Orange";
            this.OrangeButton.Name = "OrangeButton";
            this.OrangeButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button3_Click);
            // 
            // nocolorbutton
            // 
            this.nocolorbutton.Label = "No Color";
            this.nocolorbutton.Name = "nocolorbutton";
            this.nocolorbutton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button3_Click_1);
            // 
            // yellowbutton
            // 
            this.yellowbutton.Label = "Yellow";
            this.yellowbutton.Name = "yellowbutton";
            this.yellowbutton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button4_Click);
            // 
            // darkgreybutton
            // 
            this.darkgreybutton.Label = "Dark Grey";
            this.darkgreybutton.Name = "darkgreybutton";
            this.darkgreybutton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button5_Click);
            // 
            // lightGreyButton
            // 
            this.lightGreyButton.Label = "Light Grey";
            this.lightGreyButton.Name = "lightGreyButton";
            this.lightGreyButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.lightGreyButton_Click);
            // 
            // group3
            // 
            this.group3.Items.Add(this.teenyastudybutton);
            this.group3.Items.Add(this.youthclass1button);
            this.group3.Items.Add(this.youthclass24button);
            this.group3.Items.Add(this.elementarypreeteenbutton);
            this.group3.Items.Add(this.teenbutton);
            this.group3.Items.Add(this.mensclubButton);
            this.group3.Label = "Extra Text";
            this.group3.Name = "group3";
            // 
            // teenyastudybutton
            // 
            this.teenyastudybutton.Label = "Teen/YA Study";
            this.teenyastudybutton.Name = "teenyastudybutton";
            this.teenyastudybutton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.teenyastudybutton_Click);
            // 
            // youthclass1button
            // 
            this.youthclass1button.Label = "Youth Class 1";
            this.youthclass1button.Name = "youthclass1button";
            this.youthclass1button.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.youthclass1button_Click);
            // 
            // youthclass24button
            // 
            this.youthclass24button.Label = "Youth Class 2&4";
            this.youthclass24button.Name = "youthclass24button";
            this.youthclass24button.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.youthclass24button_Click);
            // 
            // elementarypreeteenbutton
            // 
            this.elementarypreeteenbutton.Label = "Elementary/Preteen Class";
            this.elementarypreeteenbutton.Name = "elementarypreeteenbutton";
            this.elementarypreeteenbutton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.teenpreeteenbutton_Click);
            // 
            // teenbutton
            // 
            this.teenbutton.Label = "Teen Class";
            this.teenbutton.Name = "teenbutton";
            this.teenbutton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.teenbutton_Click);
            // 
            // mensclubButton
            // 
            this.mensclubButton.Label = "Men\'s Club";
            this.mensclubButton.Name = "mensclubButton";
            this.mensclubButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.mensclubButton_Click);
            // 
            // group4
            // 
            this.group4.Items.Add(this.sunsetTimesV2Checkbox);
            this.group4.Items.Add(this.testButton);
            this.group4.Items.Add(this.configurationButton);
            this.group4.Label = "Options";
            this.group4.Name = "group4";
            // 
            // sunsetTimesV2Checkbox
            // 
            this.sunsetTimesV2Checkbox.Label = "sunsetTimesV2";
            this.sunsetTimesV2Checkbox.Name = "sunsetTimesV2Checkbox";
            this.sunsetTimesV2Checkbox.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.sunsetTimesV2Checkbox_Click);
            // 
            // testButton
            // 
            this.testButton.Label = "GenerateDates";
            this.testButton.Name = "testButton";
            this.testButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.testButton_Click);
            // 
            // configurationButton
            // 
            this.configurationButton.Label = "Configuration";
            this.configurationButton.Name = "configurationButton";
            this.configurationButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.configurationButton_Click);
            // 
            // tab2
            // 
            this.tab2.Groups.Add(this.group1);
            this.tab2.Label = "Background Color";
            this.tab2.Name = "tab2";
            // 
            // group1
            // 
            this.group1.Label = "group1";
            this.group1.Name = "group1";
            // 
            // SunsetRibbon
            // 
            this.Name = "SunsetRibbon";
            this.RibbonType = "Microsoft.Word.Document";
            this.Tabs.Add(this.tab1);
            this.Tabs.Add(this.tab2);
            this.Close += new System.EventHandler(this.SunsetRibbon_Close);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.SunsetRibbon_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.SunsetRibbonControl.ResumeLayout(false);
            this.SunsetRibbonControl.PerformLayout();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();
            this.group3.ResumeLayout(false);
            this.group3.PerformLayout();
            this.group4.ResumeLayout(false);
            this.group4.PerformLayout();
            this.tab2.ResumeLayout(false);
            this.tab2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup SunsetRibbonControl;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton SunsetTimeCongregation1Button;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton SunsetTimeCongregation2Button;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton SunsetTimeCongregation3Button;
        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab2;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton greenButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton blueButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton OrangeButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton nocolorbutton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton yellowbutton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton darkgreybutton;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group3;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton teenyastudybutton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton youthclass1button;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton youthclass24button;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton elementarypreeteenbutton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton teenbutton;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group4;
        internal Microsoft.Office.Tools.Ribbon.RibbonCheckBox sunsetTimesV2Checkbox;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton mensclubButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton lightGreyButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton testButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton configurationButton;
    }

    partial class ThisRibbonCollection
    {
        internal SunsetRibbon SunsetRibbon
        {
            get { return this.GetRibbon<SunsetRibbon>(); }
        }
    }
}
